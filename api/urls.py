from django.urls import path
from api import views

urlpatterns = [
    path("logs/", views.LoggingUwsgi.as_view()),
]