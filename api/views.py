from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

class LoggingUwsgi(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        return Response(get_logs())

def get_logs():
        with open("/var/www/html/logs/uwsgi-emperor.log") as file:
            read_data = file.readlines()
            return read_data
    
